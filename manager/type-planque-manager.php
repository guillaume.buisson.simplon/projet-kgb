<!-- MANAGER TYPE PLANQUE -->

<?php

require dirname(__DIR__).'/autoloader.php';
require dirname(__DIR__).'/modele/type-planques.php';

// CLASS TYPE PLANQUE
class TypePlanqueManager extends DBManager{

    public function getAll() {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM type_de_planque');

        while($row = $stmt->fetch()) {
            $typeplanque = new Typeplanque();
            $typeplanque->setCode($row['code_planque']);
            $typeplanque->setTypeplanque($row['type_planque']);

            $result[] = $typeplanque;
        }

        return $result;
    }

    public function add($typeplanque) {
        $stmt = $this->getConnexion()->prepare('INSERT INTO type_de_planque VALUES 
                                                (:code, 
                                                :typepl);');
                                                
        $stmt->execute(['code' => NULL,
                        'typepl' => $typeplanque->getTypeplanque()]);
        return true;
    }

    public function  delete($type) {

        $stmt = $this->getConnexion()->prepare('DELETE FROM type_de_planque WHERE code_planque = :code');

        $result = $stmt->execute(['code' => $type->getCode()]);

        return $result;
    }
}