<!-- MANAGER PAYS -->

<?php

require dirname(__DIR__).'/autoloader.php';
require dirname(__DIR__).'/modele/pays.php';

// CLASS PAYS
class PaysManager extends DBManager{

    public function getAll() {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM pays');

        while($row = $stmt->fetch()) {
            $pays = new Pays();
            $pays->setCodepays($row['code_pays']);
            $pays->setPays($row['pays']);

            $result[] = $pays;
        }

        return $result;
    }

    public function add($pays) {
        $stmt = $this->getConnexion()->prepare('INSERT INTO pays VALUES 
                                                (:code, 
                                                :pays)');
                                                
        $stmt->execute(['code' => NULL,
                        'pays' => $pays->getPays()]);
        return true;
    }

    public function  delete($pays) {

        $stmt = $this->getConnexion()->prepare('DELETE FROM pays WHERE code_pays = :code');

        $result = $stmt->execute(['code' => $pays->getCodepays()]);

        return $result;
    }
}