<!-- MANAGER CONTACT -->

<?php

require dirname(__DIR__).'/autoloader.php';
require dirname(__DIR__).'/modele/contacts.php';

// CLASS CONTACT
class ContactManager extends DBManager{

    public function getAll() {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM contact JOIN pays ON contact.code_pays = pays.code_pays');

        while($row = $stmt->fetch()) {
            $contact = new Contact();
            $contact->setCode($row['code_contact']);
            $contact->setNomcode($row['nom_code']);
            $contact->setNom($row['nom_contact']);
            $contact->setPrenom($row['prenom_contact']);
            $contact->setDatenaissance($row['date_naissance_contact']);
            $contact->setCodepays($row['code_pays']);
            // INFO FROM JOIN TABLE : PAYS
            $contact->setNamePays($row['pays']);

            $result[] = $contact;
        }

        return $result;
    }

    public function add($contact) {
        $stmt = $this->getConnexion()->prepare('INSERT INTO contact VALUES 
                                                (:code, 
                                                :nomcode,
                                                :nom, 
                                                :prenom, 
                                                :datena, 
                                                :codepays);');
                                                
        $stmt->execute(['code' => NULL,
                        'nomcode' => $contact->getNomcode(),
                        'nom' => $contact->getNom(),
                        'prenom' => $contact->getPrenom(),
                        'datena' => $contact->getDatenaissance(),
                        'codepays' => $contact->getCodepays()]);
        return true;
    }

    public function delete($contact) {
        $stmt = $this->getConnexion()->prepare('DELETE FROM contact WHERE code_contact = :code');

        $result = $stmt->execute(['code' => $contact->getCode()]);

        return $result;
    }
}