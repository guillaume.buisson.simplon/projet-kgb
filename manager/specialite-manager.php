<!-- MANAGER SPECIALITE -->

<?php

require dirname(__DIR__).'/autoloader.php';
require dirname(__DIR__).'/modele/specialites.php';

// CLASS SPECIALITE
class SpecialiteManager extends DBManager{

    public function getAll() {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM specialite');

        while($row = $stmt->fetch()) {
            $specialite = new Specialite();
            $specialite->setCode($row['code_spe']);
            $specialite->setSpecialite($row['specialite']);

            $result[] = $specialite;
        }

        return $result;
    }

    public function add($specialite) {
        $stmt = $this->getConnexion()->prepare('INSERT INTO specialite VALUES 
                                                (:code, 
                                                :specialite);');
                                                
        $stmt->execute(['code' => NULL,
                        'specialite' => $specialite->getSpecialite()]);
        return true;
    }

    public function  delete($spe) {

        $stmt = $this->getConnexion()->prepare('DELETE FROM specialite WHERE code_spe = :code');

        $result = $stmt->execute(['code' => $spe->getCode()]);

        return $result;
    }
}