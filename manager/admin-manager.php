<!-- MANAGER ADMIN -->

<?php

require dirname(__DIR__).'/autoloader.php';
require dirname(__DIR__).'/modele/admin.php';

// CLASS ADMIN
class AdminManager extends DBManager{

    public function getAll() {
        $result = [];

        $stmt = $this->getConnexion()->query('SELECT * FROM administrateur');

        while($row = $stmt->fetch()) {
            $admin = new Admin();
            $admin->setCodeadmin($row['code_admin']);
            $admin->setNom($row['nom_admin']);
            $admin->setPrenom($row['prenom_admin']);
            $admin->setEmail($row['email']);
            $admin->setMdp($row['mdp']);
            $admin->setDatecreation($row['date_creation']);

            $result[] = $admin;
        }

        return $result;
    }

    public function add($admin) {
        $stmt = $this->getConnexion()->prepare('INSERT INTO administrateur VALUES 
                                                (:code,
                                                :nom, 
                                                :prenom, 
                                                :email, 
                                                :mdp, 
                                                :datecrea);');
                                                
        $stmt->execute(['code' => NULL,
                        'nom' => $admin->getNom(),
                        'prenom' => $admin->getPrenom(),
                        'email' => $admin->getEmail(),
                        'mdp' => $admin->getMdp(),
                        'datecrea' => date('Y-m-d')]);
        return true;
    }
}