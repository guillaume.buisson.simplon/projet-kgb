<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="assets/dist/style.css">
</head>
<body>
    <div class="loading-screen">
        <p id="text-loading" class="pourcent-loading-bar"></p>
        <div class="border-loading-bar">
            <div id="background-bar" class="background-loading-bar"></div>
        </div>
    </div>

    <script src="assets/js/loading-screen.js"></script>
</body>
</html>
