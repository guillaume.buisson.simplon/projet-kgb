# Projet KGB

## Consignes


## Schema de la DB

####Missions
[__Nom de code__, Titre, Description, Pays, nbr d'agents, nbrs de contacts, nbr de cibles, type de mission, nbr de planques, statut de la mission, spécialité requise, date_debut, date_fin]<br/>
clé primaire(Nom de code)<br/>
clé étrangère(code agent, code cible, code contact, code planque)

####Agent
[__Code_agent__, nom, prenom, date_naissance, code_pays, code_spé]<br/>
clé primaire(Code_agent)<br/>
clé étrangère(code_pays)<br/>
clé étrangère(code_spé)

####Cible
[__Code_cible__, nom, prenom, date_naissance, code_pays]<br/>
clé primaire(Code_cible)<br/>
clé étrangère(code_pays)

####Contact
[__Code_contact__, nom, prenom, date_naissance, code_pays]<br/>
clé primaire(Code_contact)<br/>
clé étrangère(code_pays)

####Planques
[__code_planque__, adresse, pays, code_type]<br/>
clé primaire(code_planque)<br/>
clé étrangère(code_type)

####Spécialité
[__code_spe__, code_agent, code_mission]<br/>
clé primaire(code_spé)<br/>
clé étrangère(code agent, code mission)

####type_de_planque
[__code_type__, type_planque]<br/>
clé primaire(code_type)

####type_de_mission
[__code_type_mission__, type_mission]<br/>
clé primaire(code_type_mission)

####mission_necessite_contact
[__code_mission__, code_contact]<br/>
clé primaire(code mission)<br/>
clé étrangère(code_mission, code_contact)

####mission_utilise_planque
[__code_mission__, code_planque]<br/>
clé primaire(code_mission)<br/>
clé étrangère(code_mission, code_planque)

####mission_attribut_spe
[__code_mission__, code_spe]<br/>
clé primaire(code_mission)<br/>
clé étrangère(code_mission, code_spe)

####mission_vise_cible
[__code_mission__, code_cible]<br/>
clé primaire(code_mission)<br/>
clé étrangère(code_mission, code_cible)

####agent_possede_spe
[__code_agent__, code_spe]<br/>
clé primaire(code_agent)<br/>
clé étrangère(code_agent, code_spe)