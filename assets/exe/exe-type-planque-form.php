<!-- EXE FORMULAIRE TYPE PLANQUE -->

<?php 
    require '../../manager/type-planque-manager.php';

    if (!empty($_POST['type'])) {
        $mytypeplanquemanager = new TypePlanqueManager();
    
        $newtype = new Typeplanque();
        $newtype->setTypeplanque($_POST['type']);
    
        $mytypeplanquemanager->add($newtype);
        header ('location: ./../../loading-page.php');
    }
    else {
        header ('location: ./../../administration.php');
    }