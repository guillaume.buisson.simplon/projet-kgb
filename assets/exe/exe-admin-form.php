<!-- EXE FORMULAIRE ADMIN -->
<?php 
    require '../../manager/admin-manager.php';

    if (!empty($_POST['mail']) && !empty($_POST['pass'])) {
        $myadminmanager = new AdminManager();
        $cryptedmdp = password_hash($_POST['pass'], PASSWORD_BCRYPT);
    
        $newadmin = new Admin();
        $newadmin->setNom($_POST['nom']);
        $newadmin->setPrenom($_POST['prenom']);
        $newadmin->setEmail($_POST['mail']);
        $newadmin->setMdp($cryptedmdp);

        $myadminmanager->add($newadmin);
        header ('location: ./../../loading-page.php');
    }
    else {
        header ('location: ./../../administration.php');
    }    