<!-- EXE FORMULAIRE SPECIALITE -->

<?php 
    require '../../manager/specialite-manager.php';

    if (!empty($_POST['specialite'])) {
        $myspecialitemanager = new SpecialiteManager();

        $newspecialite = new Specialite();
        $newspecialite->setSpecialite($_POST['specialite']);

        $myspecialitemanager->add($newspecialite);
        header ('location: ./../../loading-page.php');
    }
    else {
        header ('location: ./../../administration.php');
    }
    