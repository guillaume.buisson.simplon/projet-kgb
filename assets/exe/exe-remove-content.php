<?php

    require '../../autoloader.php';

    if($_GET['table'] == 'mission')
    {
        $missionmanager = new MissionManager();
        $mission = new Mission();
        $mission->setCode($_GET['id']);

        $missionmanager->delete($mission);
        $result = $missionmanager->delete($mission);

        if ($result == "") {
            $result = 'false';
        }
        else {
            $result = 'deleted';
        }
        sleep(2);
        header('location: ../../index.php?result='.$result);
    }
    else if($_GET['table'] == 'agent')
    {
        $manager = new AgentManager();
        $elem = new Agent();
        $elem->setCode($_GET['id']);

        $manager->delete($elem);
        $result = $manager->delete($elem);

        if ($result == "") {
            $result = 'false';
        }
        else {
            $result = 'deleted';
        }
        sleep(2);
        header('location: ../../index.php?result='.$result);
    }
    else if($_GET['table'] == 'cible')
    {
        $manager = new CibleManager();
        $elem = new Cible();
        $elem->setCode($_GET['id']);

        $manager->delete($elem);
        $result = $manager->delete($elem);

        if ($result == "") {
            $result = 'false';
        }
        else {
            $result = 'deleted';
        }
        sleep(2);
        header('location: ../../index.php?result='.$result);
    }
    else if($_GET['table'] == 'contact')
    {
        $manager = new ContactManager();
        $elem = new Contact();
        $elem->setCode($_GET['id']);

        $manager->delete($elem);
        $result = $manager->delete($elem);

        if ($result == "") {
            $result = 'false';
        }
        else {
            $result = 'deleted';
        }
        sleep(2);
        header('location: ../../index.php?result='.$result);
    }
    else if($_GET['table'] == 'planque')
    {
        $manager = new PlanqueManager();
        $elem = new Planque();
        $elem->setCode($_GET['id']);

        $manager->delete($elem);
        $result = $manager->delete($elem);

        if ($result == "") {
            $result = 'false';
        }
        else {
            $result = 'deleted';
        }
        sleep(2);
        header('location: ../../index.php?result='.$result);
    }
    else if($_GET['table'] == 'typemission')
    {
        $manager = new TypeMissionManager();
        $elem = new Typemission();
        $elem->setCodetypemission($_GET['id']);

        $manager->delete($elem);
        $result = $manager->delete($elem);

        if ($result == "") {
            $result = 'false';
        }
        else {
            $result = 'deleted';
        }
        sleep(2);
        header('location: ../../index.php?result='.$result);
    }
    else if($_GET['table'] == 'statut')
    {
        $manager = new StatutMissionManager();
        $elem = new Statutmission();
        $elem->setCodestatut($_GET['id']);

        $manager->delete($elem);
        $result = $manager->delete($elem);

        if ($result == "") {
            $result = 'false';
        }
        else {
            $result = 'deleted';
        }
        sleep(2);
        header('location: ../../index.php?result='.$result);
    }
    else if($_GET['table'] == 'pays')
    {
        $paysmanager = new PaysManager();
        $pays = new Pays();
        $pays->setCodepays($_GET['id']);

        $paysmanager->delete($pays);
        $result = $paysmanager->delete($pays);

        if ($result == "") {
            $result = 'false';
        }
        else {
            $result = 'deleted';
        }
        sleep(2);
        header('location: ../../index.php?result='.$result);
    }
    else if($_GET['table'] == 'specialite')
    {
        $manager = new SpecialiteManager();
        $elem = new Specialite();
        $elem->setCode($_GET['id']);

        $manager->delete($elem);
        $result = $manager->delete($elem);

        if ($result == "") {
            $result = 'false';
        }
        else {
            $result = 'deleted';
        }
        sleep(2);
        header('location: ../../index.php?result='.$result);
    }
    else if($_GET['table'] == 'typeplanque')
    {
        $manager = new TypePlanqueManager();
        $elem = new Typeplanque();
        $elem->setCode($_GET['id']);

        $manager->delete($elem);
        $result = $manager->delete($elem);

        if ($result == "") {
            $result = 'false';
        }
        else {
            $result = 'deleted';
        }
        sleep(2);
        header('location: ../../index.php?result='.$result);
    }
