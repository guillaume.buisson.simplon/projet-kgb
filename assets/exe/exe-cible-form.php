<!-- EXE FORMULAIRE CIBLE -->

<?php 

    require '../../manager/cible-manager.php';    

    if (!empty($_POST['nom']) && !empty($_POST['prenom'])) {
        $myciblemanager = new CibleManager();

        $newcible = new Cible();
        $newcible->setNom($_POST['nom']);
        $newcible->setPrenom($_POST['prenom']);
        $newcible->setDatenaissance((string)$_POST['date_naissance']);
        $newcible->setCodepays((int)$_POST['pays']);
    
        $myciblemanager->add($newcible);
        header('location: ./../../loading-page.php');
    }
    else {
        header('location: ./../../administration.php');
    }
