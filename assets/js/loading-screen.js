let myloadingbar = document.getElementById('background-bar');
let textloading = document.getElementById('text-loading');
let pourcentloading = 0;

setInterval(function () {
    if(pourcentloading < 100)
    {
        pourcentloading += 10;
        myloadingbar.style.width = `${pourcentloading}%`;
        textloading.innerText = `${pourcentloading}%`;
    }

}, 200);

setTimeout(function () {
    document.location = './administration.php';
}, 2600);