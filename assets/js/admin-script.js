// SCRIPT FORM CARDS DISPLAY ON CLICK

const cardsUnclick = document.querySelectorAll(".card-unclick");

for (let cardUnclick of cardsUnclick) {

    cardUnclick.addEventListener("click", function() {

        const divAdminCard = cardUnclick.closest("#admin-card"); // Selectionne la div #admin-card

        const allDivFormCard = divAdminCard.querySelectorAll(".form-card"); // Selectionne toutes les div form-card
        // Retire la class clicked a toutes les div form-card
        for (let divFormCard of allDivFormCard) {
            divFormCard.classList.remove("clicked");
        }
        const targetDivFormCard = cardUnclick.parentNode; // Selectionne la form-card de l'élément ciblé
        // Attribue la class clicked à l'élément ciblé
        targetDivFormCard.classList.add("clicked");

        for (let cardUnclick of cardsUnclick) {
            cardUnclick.classList.remove("clicked");
        }
        cardUnclick.classList.add("clicked");

        const allDivCardClick = divAdminCard.querySelectorAll(".card-click"); // Selectionne toutes les div card-click
        // Retire la class clicked a toutes les div card-click
        for (let divCardClick of allDivCardClick) {
            divCardClick.classList.remove("clicked");
        }
        const targetDivCardClick = targetDivFormCard.querySelector(".card-click"); // Selectionne la card-click de l'élément ciblé
        // Attribue la class clicked à l'élément ciblé
        targetDivCardClick.classList.add("clicked");
        
    })
}

// SCRIPT FORM CARDS CLOSE WHEN CLICK ON BUTTON

const buttons = document.querySelectorAll(".card-close-btn"); // Variable all close button

// Pour chaque button ajoute un eventListener
for (let button of buttons) {

    // On click retire "clicked" à div form-card, div card-unclick et div card-click
    button.addEventListener("click", function(e) {

        if (e.target !== this) {

        }
        else 
        {
            // Variable div #admin-card pour ciblé all div form-card, div card-unclick et div card-click
            let divClassAdminCard = button.closest("#admin-card");
    
            //  HIDE DIV FORM-CARD
            let allDivClassCard = divClassAdminCard.querySelectorAll(".form-card"); // Variable all div card-unclick
    
            // Pour chaque div card retire la class "clicked"
            for (divClassCard of allDivClassCard) {
                divClassCard.classList.remove("clicked");
            }
    
            //  HIDE DIV CARD-UNCLICK
            let allDivClassCardUnclick = divClassAdminCard.querySelectorAll(".card-unclick"); // Variable all div card-unclick
    
            // Pour chaque div card-unclick retire la class "clicked"
            for (divClassCardUnclick of allDivClassCardUnclick) {
                divClassCardUnclick.classList.remove("clicked");
            }
    
            // HIDE DIV CARD-CLICK   
            let allDivClassCardClick = divClassAdminCard.querySelectorAll(".card-click"); // Variable all div card-unclick
    
            // Pour chaque div card-unclick retire la class "clicked"
            for (divClassCardClick of allDivClassCardClick) {
                divClassCardClick.classList.remove("clicked");
            }
        }
    })
}