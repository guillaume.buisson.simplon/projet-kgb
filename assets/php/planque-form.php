<!-- FORMULAIRE PLANQUE -->

<?php
    $mypaysmanager = new PaysManager();
    $myallpays = $mypaysmanager->getAll();

    $mytypeplanque = new TypePlanqueManager();
    $myalltypeplanque = $mytypeplanque->getAll();
?>

<form method="POST" action="./assets/exe/exe-planque-form.php">
    <label for="nom-code">Nom de code planque *</label>
    <input type="text" name="nom-code" required>
    <label for="adresse">Adresse planque</label>
    <input type="text" name="adresse">
    <div class="select-wrapper">
        <div class="select-box">
            <label for="pays">Pays</label>
            <select name="pays">
                <?php foreach($myallpays as $mypays) 
                {
                ?>
                    <option value="<?= $mypays->getCodepays(); ?>"><?= $mypays->getPays(); ?></option>
                <?php
                }
                ?>
            </select>
        </div>
        <div>
            <div class="select-box">
                <label for="type">Type</label>
                <select name="type">
                    <?php foreach($myalltypeplanque as $mytypeplanque) 
                    {
                    ?>
                        <option value="<?= $mytypeplanque->getCode(); ?>"><?= $mytypeplanque->getTypeplanque(); ?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
        </div>
    </div>
    <p class="obligatoire">* champs obligatoire</p>
    <input type="submit" value="Enregistrer" class="button">
</form>