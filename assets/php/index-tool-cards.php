<!-- TOOL CARD -->
<div class="tool-cards" id="tool-cards">

    <!-- TYPE DE MISSION CARD -->
    <div class="tool-card">
        <div class="tool-unclick">
            <?php include ("./assets/png/png-type-mission.php");?>
            <p>Type mission</p>
        </div>
        <div class="tool-click">
            <?php include ("./assets/png/png-close.php");?>
            <p>Types de mission :</p>
            <ul>
                <?php foreach($myalltype as $mytype) 
                {
                ?>
                <li>
                    <div>
                        <?= $mytype->getTypemission(); ?>
                        <?php 
                        if(isset($_SESSION['code_admin'])) {
                            ?>
                            <a href="assets/exe/exe-remove-content.php?id=<?= $mytype->getCodetypemission() ?>&table=typemission"><?php include('./assets/png/png-supprimer.php');?></a>
                            <?php
                        }
                        ?>
                    </div>
                </li>
                <?php
                }
                ?>
            </ul>
        </div>
    </div>

    <!-- STATUT MISSION CARD -->
    <div class="tool-card">
        <div class="tool-unclick">
            <?php include ("./assets/png/png-statut-mission.php");?>
            <p>Statut mission</p>
        </div>
        <div class="tool-click">
            <?php include ("./assets/png/png-close.php");?>
            <p>Statuts de mission :</p>
            <ul>
                <?php foreach($myallstatut as $mystatut) 
                {
                ?>
                <li>
                    <div>
                        <?= $mystatut->getStatutmission(); ?>
                        <?php 
                        if(isset($_SESSION['code_admin'])) {
                            ?>
                            <a href="assets/exe/exe-remove-content.php?id=<?= $mystatut->getCodestatut() ?>&table=statut"><?php include('./assets/png/png-supprimer.php');?></a>
                            <?php
                        }
                        ?>
                    </div>
                </li>
                <?php
                }
                ?>
            </ul>
        </div>
    </div>

    <!-- PAYS CARD -->
    <div class="tool-card">
        <div class="tool-unclick">
            <?php include ("./assets/png/png-pays.php");?>
            <p>Pays</p>
        </div>
        <div class="tool-click">
            <?php include ("./assets/png/png-close.php");?>
            <p>Pays couverts :</p>
            <ul>
                <?php foreach($myallpays as $mypays) 
                {
                ?>
                <li>
                    <div>
                    <?= $mypays->getPays(); ?>
                        <?php 
                        if(isset($_SESSION['code_admin'])) {
                            ?>
                            <a href="assets/exe/exe-remove-content.php?id=<?= $mypays->getCodepays() ?>&table=pays"><?php include('./assets/png/png-supprimer.php');?></a>
                            <?php
                        }
                        ?>
                    </div>
                </li>
                <?php
                }
                ?>
            </ul>
        </div>
    </div>

    <!-- SPECIALITE CARD -->
    <div class="tool-card">
        <div class="tool-unclick">
            <?php include ("./assets/png/png-specialite.php");?>
            <p>Spécialité</p>
        </div>
        <div class="tool-click">
            <?php include ("./assets/png/png-close.php");?>
            <p>Nos spécialités :</p>
            <ul>
                <?php foreach($myallspecialite as $myspecialite) 
                {
                ?>
                <li>
                    <div>
                    <?= $myspecialite->getSpecialite(); ?>
                        <?php 
                        if(isset($_SESSION['code_admin'])) {
                            ?>
                            <a href="assets/exe/exe-remove-content.php?id=<?= $myspecialite->getCode() ?>&table=specialite"><?php include('./assets/png/png-supprimer.php');?></a>
                            <?php
                        }
                        ?>
                    </div>
                </li>
                <?php
                }
                ?>
            </ul>
        </div>
    </div>

    <!-- TYPE DE PLANQUE CARD -->
    <div class="tool-card">
        <div class="tool-unclick">
            <?php include("./assets/png/png-type-planque.php");?>
            <p>Type planque</p>
        </div>
        <div class="tool-click">
            <?php include ("./assets/png/png-close.php");?>
            <p>Type de planque :</p>
            <ul>
                <?php foreach($myalltypeplanque as $mytypeplanque) 
                {
                ?>
                <li>
                    <div>
                    <?= $mytypeplanque->getTypeplanque(); ?>
                        <?php 
                        if(isset($_SESSION['code_admin'])) {
                            ?>
                            <a href="assets/exe/exe-remove-content.php?id=<?= $mytypeplanque->getCode() ?>&table=typeplanque"><?php include('./assets/png/png-supprimer.php');?></a>
                            <?php
                        }
                        ?>
                    </div>
                </li>
                <?php
                }
                ?>
            </ul>
        </div>
    </div>

</div>