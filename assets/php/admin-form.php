<!-- FORMULAIRE ADMIN -->

<form method="POST" action="assets/exe/exe-admin-form.php" class="form-administrateur">
    <label for="nom">Nom</label>
    <input type="text" name="nom">
    <label for="prenom">Prénom</label>
    <input type="text" name="prenom">
    <label for="mail">Email *</label>
    <input type="email" name="mail" required>
    <label for="pass">Mot de passe *</label>
    <input type="password" name="pass" required>
    <p class="obligatoire">* champs obligatoire</p>
    <input type="submit" value="Enregistrer" class="button">
</form>