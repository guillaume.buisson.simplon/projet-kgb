<!-- FORMULAIRE CIBLE -->

<?php
    $mypaysmanager = new PaysManager();
    $myallpays = $mypaysmanager->getAll();
?>

<form method="POST" action="./assets/exe/exe-cible-form.php">
    <label for="nom">Nom *</label>
    <input type="text" name="nom" required>
    <label for="prenom">Prenom *</label>
    <input type="text" name="prenom" required>
    <label for="date_naissance">Date naissance</label>
    <input type="date" name="date_naissance" class="date-naissance">
    <label for="pays">Pays</label>
    <select name="pays">
        <?php foreach($myallpays as $mypays) 
        {
        ?>
            <option value="<?= $mypays->getCodepays(); ?>"><?= $mypays->getPays(); ?></option>
        <?php
        }
        ?>
    </select>
    <p class="obligatoire">* champs obligatoire</p>
    <input type="submit" value="Enregistrer" class="button">
</form>