<?php 

    /*final*/ class Typeplanque {
        private int $code;
        private string $typeplanque;

        public function getCode() : int
        {
                return $this->code;
        }

        public function setCode(int $code)
        {
                $this->code = $code;

                return $this;
        }

        public function getTypeplanque() : string
        {
                return $this->typeplanque;
        }

        public function setTypeplanque(string $typeplanque)
        {
                $this->typeplanque = $typeplanque;

                return $this;
        }
    }