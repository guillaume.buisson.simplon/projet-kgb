<?php 

    /*final*/ class Missionspe {
        private int $codemission;
        private int $codespe;

        public function getCodemission() : int
        {
                return $this->codemission;
        }

        public function setCodemission(int $codemission)
        {
                $this->codemission = $codemission;

                return $this;
        }

        public function getCodespe() : int
        {
                return $this->codespe;
        }

        public function setCodespe(int $codespe)
        {
                $this->codespe = $codespe;

                return $this;
        }
    }