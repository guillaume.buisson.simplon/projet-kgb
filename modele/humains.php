<?php 

    abstract class Humain {
        private string $nom;
        private string $prenom;
        private string $datenaissance;

        public function getNom() : string
        {
                return $this->nom;
        }

        public function setNom(string $nom)
        {
                $this->nom = $nom;

                return $this;
        }

        public function getPrenom() : string
        {
                return $this->prenom;
        }

        public function setPrenom(string $prenom)
        {
                $this->prenom = $prenom;

                return $this;
        }

        public function getDatenaissance() :string
        {
                return $this->datenaissance;
        }

        public function setDatenaissance(string $datenaissance)
        {
                $this->datenaissance = $datenaissance;

                return $this;
        }
    }