<?php 

    require_once __DIR__ .'/humains.php';
    
    /*final*/ class Cible extends Humain{
        private int $code;
        private int $codepays;

        public function getCode() : int
        {
                return $this->code;
        }

        public function setCode(int $code)
        {
                $this->code = $code;

                return $this;
        }

        public function getCodepays() : int
        {
                return $this->codepays;
        }

        public function setCodepays(int $codepays)
        {
                $this->codepays = $codepays;

                return $this;
        }

        // GET & SET ONLY USE FOR DISPLAY VALUE FROM JOIN TABLE

        public function getNamePays() : string
        {
                return $this->namepays;
        }

        public function setNamePays(string $namepays)
        {
                $this->namepays = $namepays;

                return $this;
        }
    }