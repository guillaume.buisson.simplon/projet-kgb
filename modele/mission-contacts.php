<?php 

    /*final*/ class Missioncontact {
        private int $codemission;
        private int $codecontact;

        public function getCodemission() : int
        {
                return $this->codemission;
        }

        public function setCodemission(int $codemission)
        {
                $this->codemission = $codemission;

                return $this;
        }

        public function getCodecontact() : int
        {
                return $this->codecontact;
        }

        public function setCodecontact(int $codecontact)
        {
                $this->codecontact = $codecontact;

                return $this;
        }
    }