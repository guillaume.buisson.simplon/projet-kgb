<?php 

    /*final*/ class Nationalite {
        private int $code;
        private string $nationalite;

        public function getCode() : int
        {
                return $this->code;
        }

        public function setCode(int $code)
        {
                $this->code = $code;

                return $this;
        }

        public function getNationalite() : string
        {
                return $this->nationalite;
        }

        public function setNationalite(string $nationalite)
        {
                $this->nationalite = $nationalite;

                return $this;
        }
    }